bashCalcReplaceSpecialChars() {
  calc="$@"
  calc="${calc//x/*}"
  calc="${calc//[/\(}"
  calc="${calc//]/\)}"
  calc="${calc//o/\(}"
  calc="${calc//c/\)}"

  echo "$calc"
}

bashCalcGetInterpreter() {
  if [[ $(which gcalccmd) ]]; then
    echo "gcalccmd"
  elif [[ $(which bc) ]]; then
    echo "bc"
  else
    echo "shell"
  fi
}

bashCalcHelp() {
  echo "Compute operation using (in order) gcalccmd, bc or shell interpreter"
  echo "Interpreter currently used is: "`bashCalcGetInterpreter`
}

=() {

  if [[ "help" == "$1" || "-help" == "$1" || "--help" == "$1"  || "-h" == "$1" ]]; then
    bashCalcHelp
    return
  fi

  if [[ "gcalccmd" ==  $(bashCalcGetInterpreter) ]]; then
    echo -ne "`bashCalcReplaceSpecialChars $@`\n quit" | gcalccmd | sed 's:^> ::g';
  elif [[ "bc" ==  $(bashCalcGetInterpreter) ]]; then
    echo -e "`bashCalcReplaceSpecialChars $@`" | bc -l | sed '/\./ s/\.\{0,1\}0\{1,\}$//'
  else 
    echo -e "$((`bashCalcReplaceSpecialChars $@`))"
  fi

}
