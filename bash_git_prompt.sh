#!/bin/bash

#          RED="\[\033[0;31m\]"
#    LIGHT_RED="\[\033[1;31m\]"
#        GREEN="\[\033[0;32m\]"
#  LIGHT_GREEN="\[\033[1;32m\]"
#       YELLOW="\[\033[0;33m\]"
# LIGHT_YELLOW="\[\033[1;33m\]"
#         BLUE="\[\033[0;34m\]"
#   LIGHT_BLUE="\[\033[1;34m\]"
#      MAGENTA="\[\033[0;35m\]"
#LIGHT_MAGENTA="\[\033[1;35m\]"
#         GRAY="\[\033[0;37m\]"
#        WHITE="\[\033[1;37m\]"
#   COLOR_NONE="\[\e[0m\]"

function set_term_title {
    echo -ne "\033]0;$1\007"
}

function parse_git_branch {
    location=`whoami`@`hostname`

    previous_return_value=$?;

    upToDate=1
	
    git_status="$(git status 2> /dev/null)"
    git_stash="$(git stash list 2> /dev/null)"
    branch_pattern="^On branch ([^${IFS}]*)"
    
    if [ $previous_return_value -eq 0 ] \
      || [ $previous_return_value -eq 141 ] \
      || [ "false" == "$GIT_PROMPT_SHOW_ERROR" ]; then
        p="${GIT_COLOR_DEFAULT} \$ ${GIT_COLOR_DEFAULT}"
    else
        p=" ${GIT_COLOR_COMMAND_FAILED}⚡${GIT_COLOR_DEFAULT} "
    fi

    
    if [[ ${git_status} =~ ${branch_pattern} ]]; then
        # branch name
        branch="${GIT_COLOR_BRANCH}${BASH_REMATCH[1]}${GIT_COLOR_DEFAULT}"

        # Changes to be committed
        to_be_commited=
        if [[ ${git_status}} =~ "Changes to be committed" ]]; then
            to_be_commited=" ${GIT_COLOR_TO_BE_COMMITED_ON}◉${GIT_COLOR_DEFAULT}"
			upToDate=0
        elif [[ "true" == "$GIT_PROMPT_INFO" ]]; then
            to_be_commited=" ${GIT_COLOR_TO_BE_COMMITED_OFF}◌${GIT_COLOR_DEFAULT}"
        fi

        # Changes not staged for commit
        not_staged=
        if [[ ${git_status}} =~ "Changes not staged for commit" ]]; then
            not_staged=" ${GIT_COLOR_NOT_STAGED_ON}◉${GIT_COLOR_DEFAULT}"
			upToDate=0
        elif [[ "true" == "$GIT_PROMPT_INFO" ]]; then
            not_staged=" ${GIT_COLOR_NOT_STAGED_OFF}◌${GIT_COLOR_DEFAULT}"
        fi

        # Untracked files
        untracked=
        if [[ ${git_status}} =~ "Untracked files" ]]; then
            untracked=" ${GIT_COLOR_UNTRACKED_ON}❓${GIT_COLOR_DEFAULT}"
			upToDate=0
        elif [[ "true" == "$GIT_PROMPT_INFO" ]]; then
            untracked=" ${GIT_COLOR_UNTRACKED_OFF}❔${GIT_COLOR_DEFAULT}"
        fi

        # Stash
        stash=
        if [[ ${git_stash} =~ "stash" ]]; then
            stash=" ${GIT_COLOR_STASH_ON}⤤${GIT_COLOR_DEFAULT}"
			upToDate=0
        elif [[ "true" == "$GIT_PROMPT_INFO" ]]; then
            stash=" ${GIT_COLOR_STASH_OFF}⤤${GIT_COLOR_DEFAULT}"
        fi

        # Sync with remote
        remote=
        if [[ ${git_status} =~ "Your branch is ahead" ]]; then
            remote=" ${GIT_COLOR_REMOTE_AHEAD}↑${GIT_COLOR_DEFAULT}"
            upToDate=0
        elif [[ ${git_status} =~ "Your branch is behind" ]]; then
            remote=" ${GIT_COLOR_REMOTE_BEHIND}↓${GIT_COLOR_DEFAULT}"
            upToDate=0
        elif [[ ${git_status} =~ "have diverged" ]]; then
            remote=" ${GIT_COLOR_REMOTE_DIVERDED}↕${GIT_COLOR_DEFAULT}"
            upToDate=0
        elif [[ "true" == "$GIT_PROMPT_INFO" ]]; then
            remote=" ${GIT_COLOR_REMOTE_SYNC}✔${GIT_COLOR_DEFAULT}"
        fi

#        if [ 1 -eq $upToDate ] && [ "false" == "$GIT_PROMPT_INFO" ]; then
#            upToDate=" ${GIT_COLOR_UP_TO_DATE}✔${GIT_COLOR_UP_TO_DATE}"
#        else
#            upToDate=" "
#        fi
        upToDate="" 
        PS1="${GIT_COLOR_TIME}\t ${GIT_COLOR_LOCATION}$location:${GIT_COLOR_PATH}\w ${GIT_COLOR_BRACKETS}[${branch}${to_be_commited}${not_staged}${untracked}${stash}${remote}${upToDate}${GIT_COLOR_BRACKETS}]${p}"
    else
        PS1="${GIT_COLOR_TIME}\t ${GIT_COLOR_LOCATION}$location:${GIT_COLOR_PATH}\w${p}"
    fi
    set_term_title $location
}

GIT_COLOR_DEFAULT=$(c_color -e)

GIT_COLOR_TIME=$(c_color -e -f magenta)

GIT_COLOR_LOCATION=$(c_color -e -f white)
GIT_COLOR_PATH=$(c_color -e -B -f white)
GIT_COLOR_COMMAND_FAILED=$(c_color -e -f red -B)

GIT_COLOR_BRACKETS=$(c_color -e -f blue)

GIT_COLOR_BRANCH=$(c_color -e -f blue -B)

GIT_COLOR_TO_BE_COMMITED_ON=$(c_color -e -B -f green)
GIT_COLOR_TO_BE_COMMITED_OFF=$(c_color -e -f green)

GIT_COLOR_NOT_STAGED_ON=$(c_color -e -f red -B)
GIT_COLOR_NOT_STAGED_OFF=$(c_color -e -f red)

GIT_COLOR_UNTRACKED_ON=$(c_color -e -f red -B)
GIT_COLOR_UNTRACKED_OFF=$(c_color -e -f white)

GIT_COLOR_STASH_ON=$(c_color -e -f yellow -B)
GIT_COLOR_STASH_OFF=$(c_color -e -f white)

GIT_COLOR_REMOTE_SYNC=$(c_color -e -f green)
GIT_COLOR_REMOTE_AHEAD=$(c_color -e -f yellow)
GIT_COLOR_REMOTE_BEHIND=$(c_color -e -f yellow)
GIT_COLOR_REMOTE_DIVERDED=$(c_color -e -f red)

GIT_COLOR_UP_TO_DATE=$(c_color -e -f green)

GIT_PROMPT_INFO=false
GIT_PROMPT_SHOW_ERROR=true

PROMPT_COMMAND=parse_git_branch
