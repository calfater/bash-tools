#!/bin/bash

PATTERN='{artist}/{album}/{track-}{title}.{ext}'
BR=256
SR=44.10

checkIfBinIsInstalled() {
  bin=$1
  [[ ! -z $2 ]] && apt=$2 || apt=$1
  [[ ! $(which $1) ]] && echo -e "The program '$1' is currently not installed. You can install it by typing:\nsudo apt install $apt" &&  exit 1
}

getId3FileInfoInit() {
  file=$1
  eyed3_cache_data="$(eyeD3 -v  --strict  "$file" 2>&1)"
}

getId3FileInfo() {
  file=$1
  type=$2
  case $type in
    "artist") type="TPE1" ;;
    "album")  type="TALB" ;;
    "title")  type="TIT2" ;;
    "track")  type="TRCK" ;;
    "year")   type="TYER" ;;
    *) echo "Invalid id3 type $type" ;;
  esac
  echo "$eyed3_cache_data" | grep --text -e '('"$type"')' | sed -e 's/.*)://g' -e 's/>$//' -e 's/^ *//;s/ *$//g'
#  eyeD3 --no-tagging-time-frame -v -2 -n 4 -N 15  --strict
}

removeSpecialChars() {
  echo "$@" | sed -e "s/'/ /g" -e "s/_/ /g" -e "s/-/ /g" | iconv -f utf8 -t ascii//TRANSLIT
}

toLower() {
  echo "$@" | iconv -f utf8 -t ascii//TRANSLIT | sed -e 's/\(.*\)/\L\1/' -e 's/[-()]/ /g' -e 's/[^0-9a-z ]*//g' -e 's/  */ /g' -e 's/^ *//;s/ *$//g'
}

capitalizeFirstLetters() {
    words="$@"
    echo "${words[@]^}"
}
capitalizeFirstLetter() {
    words="$@"
    echo "${words[@]^}"
}

replace() {
  str="$1"
  type="$2"
  rep="$3"
#  echo ----  $str ---- $type ---- $rep ---- 1>&2
#  echo "=> $str" | sed -e "s/\({$type\)\([^}]*\)}/$rep\\2/g" 1>&2

  if [[ "" != "$rep" ]]; then
    echo "$str" | sed -e "s/\({$type\)\([^}]*\)}/$rep\\2/g"
  else
    echo "$str" | sed -e "s/\({$type\)\([^}]*\)}//g"
  fi
}

checkIfBinIsInstalled eyeD3 eyed3
checkIfBinIsInstalled lame lame
checkIfBinIsInstalled glyrc glyrc

pattern=$PATTERN
br=$BR
sr=$SR
encode=0
dryRun=0
strip=1
move=0
verbose=0
cover=""
params=("$@")
for idx in "${!params[@]}"; do
  k="${params[$idx]}"
  v="${params[$(expr $idx + 1)]}"
  case $k in
    -h|--help    )                       help=1                       ;;
    -f|--from    ) idx=$(expr $idx + 1); src=$(readlink -f "$v")      ;;
    -t|--to      ) idx=$(expr $idx + 1); dst=$(readlink -f "$v")      ;;
    -m|--move    )                       move=1                       ;;
    -k|--keep    )                       strip=0                      ;;
    -v|--verbose )                       verbose=$(expr $verbose + 1) ;;
    -A|--artist  ) idx=$(expr $idx + 1); forceArtist=$v               ;;
    -a|--album   ) idx=$(expr $idx + 1); forceAlbum=$v                ;;
    -y|--year    ) idx=$(expr $idx + 1); forceYear=$v                 ;;
    -p|--pattern ) idx=$(expr $idx + 1); pattern=$v                   ;;
    -e|--encode  )                       encode=1                     ;;
    -d|--dry-run )                       dryRun=1                     ;;
    -c|--cover   ) idx=$(expr $idx + 1); cover=$v                     ;;
  esac
  idx=$(expr $idx + 1)
done

if [[ 0 -lt $verbose ]]; then
    echo "    help : $help"
    echo "     src : $src"
    echo "     dst : $dst"
    echo "   strip : $strip"
    echo "    move : $move"
    echo "  artist : $forceArtist"
    echo "  artist : $forceAlbum"
    echo "    year : $forceYear"
    echo " pattern : $pattern"
    echo "  dryRun : $dryRun"
    echo "  encode : $encode"
    echo "   cover : $cover"
    echo
fi

if [[ $help || ! $src || ! $dst ]]; then
  echo "It is safe to use same src and dst because a tmp file is used during the process"
  echo "eg: bin -f same/path -t same/path --cover file:///path/to/the/cover.png"
  echo ""
  echo "GENERAL"
  echo "    -h, --help"
  echo "        this help"
  echo "    -f, --from"
  echo "        source folder"
  echo "    -t, --to"
  echo "        target folder"
  echo "    -m, --move"
  echo "        source file is removed after processing"
  echo "    -v, --verbose"
  echo "        Be verbose"
  echo "    -d, --dry-run"
  echo "        do nothing"
  echo ""
  echo "ENCODING"
  echo "    -e, --encode"
  echo "        encode the file with a bitrate of ${BR} kbits/s and sample frequency at ${SR} kHz"
  echo ""
  echo "TAGGING"
  echo "    -k, --keep"
  echo "        Keep file tags"
  echo "        By default all tags are removed and only v2 artist/album/year/track/cover are set"
  echo "    -A, --artist"
  echo "        Force the artist"
  echo "    -a, --album"
  echo "        Force the album"
  echo "    -y, --year"
  echo "        Force the year"
  echo "    -c, --cover"
  echo "        a cover file path like file:///path/to/cover.png or http://server/the/cover.png"
  echo ""
  echo "NAMING"
  echo "    -p, --pattern"
  echo "        the output filename pattern"
  echo "        ($PATTERN)"
 exit 0;
fi

rm -rf /tmp/*.cover_cache
rm -rf /tmp/FRONT_COVER*

find "$src" -iname '*.mp3' | sort | while IFS= read -r file; do
  if [[ ! -f $file ]]; then break; fi
  getId3FileInfoInit "$file"
  if [[ "$forceArtist" ]]; then artist="$forceArtist"; else artist="`getId3FileInfo "$file" "artist"`"; fi
  if [[ "$forceAlbum"  ]]; then album="$forceAlbum";   else album="`getId3FileInfo "$file" "album"`";   fi
  if [[ "$forceTitle"  ]]; then title="$forceTitle";   else title="`getId3FileInfo "$file" "title"`";   fi
  if [[ "$forceTrack"  ]]; then track="$forceTrack";   else track="`getId3FileInfo "$file" "track" | sed -e 's#/.*##'`";   fi
  if [[ "$forceYear"   ]]; then year="$forceYear";     else year="`getId3FileInfo "$file" "year"`";     fi

  if [[ ! "$title"  ]]; then
    title="$(echo $file | sed -e 's=.*/==g' -e 's/\..*$//' -e 's/^[^a-zA-Z]*//g')"
  fi
  if [[ ! "$track"  ]]; then
    track="$(echo $file | sed -e 's=.*/==g' -e 's/\..*$//' -e 's/[^0-9]*//g')"
  fi
#  echo "Using artist : '$artist'"
#  echo "Using track :  '$track'"
#  echo "Using title :  '$title'"

  if   [[ ! "$artist" ]]; then echo "$file : Missing artist"
  else
    to="$dst/$pattern";
    toTmp="$dst/.tmpMp3File";
    if [[ "" !=  "$track" ]]; then
        to=$(replace "$to" "track" $(printf "%02d" $(echo $track | sed 's/^0*//')))
    else
        to=$(replace "$to" "track" "")
    fi
    to=$(replace "$to" "artist" "$(toLower $(removeSpecialChars "$artist"))")
    to=$(replace "$to" "album" "$(toLower $(removeSpecialChars "$album"))")
    to=$(replace "$to" "title" "$(toLower $(removeSpecialChars "$title"))")
    to=$(replace "$to" "ext" "mp3")


    coverPath=""
    coverPatern="/tmp/${artist} ${album} cover"
    coverCached="/tmp/$artist ${album}.cover_cache"
    if [[ ! -f "$coverCached" ]]; then # init cache
        eyeD3 --write-images=/tmp/ "$file" > /dev/null
        coverFromMp3=$(ls /tmp/FRONT_COVER.* 2> /dev/null)
        if [[ "" != "$cover" ]]; then
            curl -s "$cover" -o "${coverPatern}."${cover##*.}""
        elif [[ -f "$coverFromMp3" ]]; then
            mv "$coverFromMp3" "${coverFromMp3/FRONT_COVER/${artist} ${album} cover}"
        else
            glyrc cover --artist "$artist" --album "$album" --write "${coverPatern}.:format:" 1>/dev/null 2>&1
        fi
        touch "$coverCached"
    fi
    coverPath="$(ls "$coverPatern"* 2>/dev/null)"

    printf "%-80s -> (%s%s%s) %s" \
        "$(echo $file | cut -c -80)" \
        $(if [[ 1 -eq $strip ]]; then echo s; else echo "_"; fi; ) \
        $(if [[ 1 -eq $encode ]]; then echo e; else echo "_"; fi; ) \
        $(if [[ 1 -eq $move ]]; then echo m; else echo "_"; fi; ) \
        "$to"

    if [[ -f "$coverPath" ]]; then
        printf "\n"
    else
        printf " (no cover)\n"
    fi

    if [[ 1 -eq $dryRun ]]; then
        continue;
    fi

    mkdir -p "$(dirname "$to")"
    if [[ 1 -eq $encode ]]; then
        lameOpts=""
        if [[ 0 -eq $verbose ]]; then
            lameOpts="$lameOpts --quiet"
        fi
        lame $lameOpts -b $br --resample $sr -m s -h "$file" "$toTmp"
    else
        cp "$file" "$toTmp"
    fi

    if [[ 1 -eq $strip ]]; then eyeD3 --remove-all "$toTmp" 2> /dev/null 1 > /dev/null; fi

                                   eyeD3 -2 --artist "$(capitalizeFirstLetters "$artist")" "$toTmp" > /dev/null 2>&1
                                   eyeD3 -2 --album  "$(capitalizeFirstLetter  "$album")"  "$toTmp" > /dev/null 2>&1
                                   eyeD3 -2 --title  "$(capitalizeFirstLetter  "$title")"  "$toTmp" > /dev/null 2>&1
    if [[ "" != "$year"   ]]; then eyeD3 -2 --year      "$year"                            "$toTmp" > /dev/null 2>&1; fi
    if [[ "" != "$track"  ]]; then eyeD3 -2 --track     "$track"                           "$toTmp" > /dev/null 2>&1; fi
    if [[ -f "$coverPath" ]]; then eyeD3 -2 --add-image "$coverPath":FRONT_COVER           "$toTmp" > /dev/null 2>&1; fi

    if [[ 0 -lt $verbose  ]]; then eyeD3                                                   "$toTmp"; fi

    if [[ 1 -eq $move ]]; then
      rm "$file"
      rmdir -p --ignore-fail-on-non-empty "$(dirname "$file")"
    fi
    mv "$toTmp" "$to"
  fi

done