# A cool bash shell

> Here is a list of script I use to enchanced may shell.
> Those I wrote are available in this repo. For the others I didn't wrote I add a link to the developper website.


## Common

> This script is used by all others.

    echo >> ~/.bashrc
    wget -O - https://raw.githubusercontent.com/calfater/bash/master/bash_common_colors.sh > ~/.bash_common_colors.sh
    echo source ~/.bash_common_colors.sh >> ~/.bashrc

## Git prompt

    wget -O - https://raw.githubusercontent.com/calfater/bash/master/bash_git_prompt.sh > ~/.bash_git_prompt.sh
    echo source ~/.bash_git_prompt.sh >> ~/.bashrc

### Configuration

Modify

  * `GIT_PROMPT_INFO=false` to always display all indicators
  * `GIT_PROMPT_SHOW_ERROR=true` to change the last char of the prompt if te last command didn't success

## Mvn color log

    wget -O - https://raw.githubusercontent.com/calfater/bash/master/bash_mvn_color.sh > ~/.bash_mvn_color.sh
    echo source ~/.bash_mvn_color.sh >> ~/.bashrc

## Git auto-completion

    echo source /usr/share/bash-completion/completions/git >> ~/.bashrc

## Mvn auto-completion

> A very good script can be found at [https://github.com/bellingard/dotfiles/blob/master/maven/bash_completion.bashrc](https://github.com/bellingard/dotfiles/blob/master/maven/bash_completion.bashrc)

    wget -O - https://raw.githubusercontent.com/bellingard/dotfiles/master/maven/bash_completion.bashrc > ~/.bash_mvn_completion.sh
    echo source ~/.bash_mvn_completion.sh >> ~/.bashrc

## Git configuration

    git config --global user.name me
    git config --global user.email me@mbox.org

    git config --global credential.helper store
    git config --global push.default simple
    git config --global branch.autosetuprebase always
    git config --global status.showuntrackedfiles all
    # git config --global http.sslverify false
    
    git config --global alias.s status
    git config --global alias.l "log --graph --pretty=format:'%C(reset)%C(auto)%h%C(reset) %x09 %C(blue)[%an]%C(reset) %C(auto)%d%C(reset) %s %C(bold green) %cd %C(reset)%C(green)(%cr)' --abbrev-commit"
    git config --global alias.ll "log --graph --pretty=format:'%n%C(reset)%C(auto)%H%C(reset) %x09 %C(blue)[%an]%C(reset) %C(auto)%d%C(reset) %s%n%C(bold green)%cd %C(reset)%C(green)(%cr)%n' --abbrev-commit --name-status"
    git config --global alias.lll "log -p --graph --pretty=format:'%n%C(reset)%C(auto)%H%C(reset) %x09 %C(blue)[%an]%C(reset) %C(auto)%d%C(reset) %s%n%C(bold green)%cd %C(reset)%C(green)(%cr)%n' --abbrev-commit --color-words"
    git config --global alias.alias "\!git config --global -l | grep alias | cut -c 7-"
    git config --global alias.diff2 "diff --color-words"

## Pygmentize

[Pygments](http://pygments.org/docs/cmdline/) is a syntax highlighter

Install it using `sudo apt install python-pygments`

    echo alias catc=pygmentize >> ~/.bashrc

## Calculator

A command line shortcut calculator

ex: `= 1 + [ 2 x 3 ]`

    wget -O - https://raw.githubusercontent.com/calfater/bash/master/bash_calc.sh > ~/.bash_calc.sh
    echo source ~/.bash_calc.sh >> ~/.bashrc

## Free cash and swap

    wget -O - https://raw.githubusercontent.com/calfater/bash/master/mem.sh >> ~/.bashrc
