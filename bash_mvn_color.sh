maven_command="command mvn"

function maven_color() {
    local INFO=$(c_color -c white)
    local ERROR=$(c_color -c red)
    local WARN=$(c_color -c yellow)
    local SUCCESS=$(c_color -c green)
    local BUILDING=$(c_color -c blue -B)
    local NETWORK=$(c_color -c blue)
    local TESTS=$(c_color -c blue)
    local RUNNING=$(c_color -c blue)
    local INSTALLING=$(c_color -c blue)
    local RESET=$(c_color)
        
    $maven_command "$@" | sed \
        -e "s/\(\[INFO\] [^-].*\)/$INFO\1$RESET/g" \
        -e "s/\(\[INFO\] -.*\)/$INFO\1$RESET/g" \
        -e "s/\(\[INFO\] Building.*\)/$BUILDING\1$RESET/g" \
        -e "s/\(\[INFO\] Installing.*\)/$INSTALLING\1$RESET/g" \
        -e "s/\(\[WARNING\].*\)/$WARN\1$RESET/g" \
        -e "s/\(\[ERROR\].*\)/$ERROR\1$RESET/g" \
        -e "s/\(\(BUILD \)\{0,1\}FAILURE.*\)/$ERROR\1$RESET/g" \
        -e "s/\(\(BUILD \)\{0,1\}SUCCESS.*\)/$SUCCESS\1$RESET/g" \
        -e "s/\(Downloading:\)\(.*\)/$NETWORK\1$RESET\2/g" \
        -e "s/\(Running.*\)/$RUNNING\1$RESET/g" \
        -e "s/Tests run: \([^,]*\), Failures: \([^,]*\), Errors: \([^,]*\), Skipped: \([^,]*\)/${TESTS}Tests run: $SUCCESS\1$RESET, Failures: $ERROR\2$RESET, Errors: $WARN\3$RESET, Skipped: $LIGHT_INFO\4$RESET/g"
    return $PIPESTATUS
}

alias mvn=maven_color
