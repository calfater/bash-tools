#!/bin/bash
LATEST_DIR=latest
HISTO_DIR=backup
DATE=$(date +%Y%m%d%H%M%S)
RSYNC_DEFAULT_OPTIONS=(--protect-args --recursive -a --delete --delete-excluded --backup)

function rsw_join { local IFS=","; echo "$*"; }
function rsw_split { IFS="," read -r -a $2 <<< "$1"; }

rsw_color() {
    colorName=$1
    if [[ "$colorName" == "black"   ]]; then echo 0; fi
    if [[ "$colorName" == "red"     ]]; then echo 1; fi
    if [[ "$colorName" == "green"   ]]; then echo 2; fi
    if [[ "$colorName" == "yellow"  ]]; then echo 3; fi
    if [[ "$colorName" == "blue"    ]]; then echo 4; fi
    if [[ "$colorName" == "magenta" ]]; then echo 5; fi
    if [[ "$colorName" == "cyan"    ]]; then echo 6; fi
    if [[ "$colorName" == "white"   ]]; then echo 7; fi
}


rsw_echo() {
    msg=$1
    level=$2
    bgColor=$3
    fgColor=$4
    bold=$5

    if [[ "" == "$level" ]]; then level="0"; fi
    if [[ "" == "$bold" ]]; then bold="0"; fi

    if [[ $level -gt $verbose ]]; then
        return
    fi

    tput sgr0
#    if [[ "" != "$bgColor" ]]; then tput setab $(expr $(rsw_color $bgColor) + 8 \* $bold); fi
    if [[ "" != "$fgColor" ]]; then tput setaf $(expr $(rsw_color $fgColor) + 8 \* $bold); fi
    echo -en "$msg"
    tput sgr0
}

rsw_exec() {
    cmd=""
    for arg in "$@"; do
        if [[ $arg != "" ]];then
            cmd="$cmd '$arg'"
        fi
    done
    rsw_echo "$cmd\n" 1 "" black 1>&2
    eval $cmd
}

rsw_remove_trailing_slash() {
    echo -n $1 | sed -e 's#/*$##g'
}

pw_dir_exists() {
    dir="$1"
    sshKey="$2"
    sshPort="$3"

    if [[ "$src" = *"@"* ]]; then
        host=$(echo $dir | sed 's/:.*//')
        path=$(echo $dir | sed 's/.*://')
	result=`rsw_exec ssh $([[ "$sshKey" == "" ]] || echo -i "$sshKey") -p "$sshPort" "$host" "if [[ -d \"$path\"  ]]; then echo 1; else echo 0; fi;"`
        if [[ 0 -eq $? ]]; then
            rsw_echo "success : pw_dir_exists($dir) : '$result'\n" 2 green 1>&2
            echo $result
        else
            rsw_echo "error : pw_dir_exists($dir) : $result\n" 2 red 1>&2
            echo 0
        fi
    else
        if [[ -d "$dir" ]]; then
            echo 1
        else
            echo 0
         fi
    fi
}
pw_dir_contains_data() {
    dir="$1"
    sshKey="$2"
    sshPort="$3"

    if [[ "$dir" = *"@"* ]]; then
        d="$(echo "$dir" | sed 's/.*://')"
        result=`rsw_exec ssh $([[ "$sshKey" == "" ]] || echo -i "$sshKey") -p "$sshPort" "$(echo $dir | sed 's/:.*//')" "if [[ 0 -eq \\$(ls -A \"$d\" | wc -l)  ]]; then echo 0; else echo 1; fi;"`
        if [[ 0 -eq $? ]]; then
            rsw_echo "success : pw_dir_contains_data($dir) : '$result'\n" 2 green 1>&2
            echo $result
        else
            rsw_echo "error : pw_dir_contains_data($dir) : $result\n" 2 red 1>&2
            echo 0
        fi

    else
        if [[ -z "$(ls -A "$src")"  ]]; then
            echo 0
        else
            echo 1
         fi
    fi
}

rsw_get_size() {
    dir=$1
    excludes="$2"
    sshKey="$3"
    sshPort="$4"

    opts=""
    if [[ 1 ==  "$humanReadable" ]]; then opts="$opts -h"; fi
    for exclude in "${excludes[@]}"; do
        if [[ "$exclude" ]]; then opts="$opts --exclude '${exclude}'"; fi
    done

    if [[ "$dir" = *"@"* ]]; then
        d="$(echo "$dir" | sed 's/.*://')"
        result=`rsw_exec ssh $([[ "$sshKey" == "" ]] || echo -i "$sshKey") -p "$sshPort" "$(echo $dir | sed 's/:.*//')" "du -s $opts \"$d\""`
        if [[ 0 -eq $? ]]; then
            rsw_echo "success : rsw_get_size($dir) : '$result'\n" 2 green 1>&2
        else
            rsw_echo "error : rsw_get_size($dir) : $result\n" 2 red 1>&2
        fi
        echo "$(echo $result | sed -e 's/\s.*//')B"
    else
        echo "$(rsw_exec du -s $opts "$dir" | sed -e 's/\s.*//')B"
        # echo $(du -s $opts "$dir" | sed -e 's/\s.*//')B
    fi
}

rsw_bkp() {
    src=$1
    rsw_split "$2" dsts
    rsw_split "$3" excludes
    rsw_split "$4" rsyncOptions
    sshKey=$5
    sshPort="$6"

    sshOpts="ssh"
    if [[ "" != "$sshKey" ]]; then
        sshOpts="$sshOpts -i $sshKey"        
    fi
    if [[ "" != "$sshPort" ]]; then
        sshOpts="$sshOpts -p $sshPort"       
    fi
    rsyncOptions+=("-e" "$sshOpts")
    for exclude in "${excludes[@]}"; do
        if [[ "$exclude" ]]; then rsyncOptions+=("--exclude"); rsyncOptions+=("${exclude}"); fi
    done

    rsyncOptions+=("--backup-dir")
    rsyncOptions+=("$dst/$HISTO_DIR/$DATE")

#    rsw_exec mkdir -p "$dst/$HISTO_DIR/$DATE"
    rsw_echo " ("
    rsw_echo "$(rsw_get_size "$dst/$LATEST_DIR" "$excludes"  "$sshKey" "$sshPort")" 0 "" white
    rsw_exec rsync "${RSYNC_DEFAULT_OPTIONS[@]}" "${rsyncOptions[@]}" "$src/" "$dst/$LATEST_DIR"
    rsw_echo " -> $(rsw_get_size "$dst/$LATEST_DIR" "$excludes" "$sshKey" "$sshPort"))\n"
    if [[ 1 -lt $(find "$dst/$HISTO_DIR/" -name "*" | wc -l) ]]; then
      rmdir --ignore-fail-on-non-empty "$dst/$HISTO_DIR/"*
    fi
}

rsw_bkps() {
    id=$1
    src=$2
    rsw_split "$3" dsts
    rsw_split "$4" excludes
    rsw_split "$5" rsyncOptions
    sshKey=$6
    sshPort=$7

    rsw_echo "Backup of "
    rsw_echo "$id\n" 0 blue

    if [[ 0 -eq $(pw_dir_exists "$src" "$sshKey" "$sshPort") ]]; then
        rsw_echo " \`- $src (src not found)\n" "" yellow
    elif [[ 0 -eq $(pw_dir_contains_data "$src" "$sshKey" "$sshPort") ]]; then
        rsw_echo " \`- $src (src is empty)\n" "" red
    else
        rsw_echo " \`- $src ($(rsw_get_size "$src" "$excludes" "$sshKey" "$sshPort"))\n" 0 "" blue
        for dstIdx in "${!dsts[@]}"; do
            dst="${dsts[$dstIdx]}"
            if [[ $dstIdx != $(expr ${#dsts[@]} - 1) ]]; then
                treeBranch="  |- "
            else
                treeBranch="  \`- "
            fi
            if [[ ! -d "$dst" ]]; then
                rsw_echo "$treeBranch$dst (not found)\n" 0 "" yellow
            elif [[ ! -d "$dst/$LATEST_DIR" || ! -d "$dst/$HISTO_DIR" ]]; then
                rsw_echo "$treeBranch$dst (not initialized : missing '$LATEST_DIR' and/or '$HISTO_DIR' dir)\n" 0 "" red
            else
                rsw_echo "$treeBranch$dst"
                rsw_bkp "$src" "$(rsw_join "${dsts[@]}")" "$(rsw_join "${excludes[@]}")" "$(rsw_join "${rsyncOptions[@]}")" "$sshKey" "$sshPort"
            fi
        done
    fi
}

rsw_help() {
    rsw_echo "NAME\n" 0 "" white 1
    rsw_echo "        ${0##*/} - rsync wrapper\n\n" 0 "" white 1
    rsw_echo "SYNOPSIS\n" 0 "" white 1
    rsw_echo "        ${0##*/}\n" 0 "" white 0
    rsw_echo "        ${0##*/} [-f file] [-v] [-h] [-i | -l]\n" 0 "" white 0
    rsw_echo "        ${0##*/} --help\n\n" 0 "" white 0
    rsw_echo "DESCRIPTION\n" 0 "" white 1
    rsw_echo "        -f, --file\n" 0 "" white 1
    rsw_echo "                Set an alternative config file. Default is \$HOME/.rsw.cfg\n\n" 0 "" white 0
    rsw_echo "        -v, --verbose\n" 0 "" white 1
    rsw_echo "                Increase verbosity. Can be used 2 times\n\n" 0 "" white 0
    rsw_echo "        -h, --human-readable\n" 0 "" white 1
    rsw_echo "                Human readable sizes\n\n" 0 "" white 0
    rsw_echo "        -i, --id\n" 0 "" white 1
    rsw_echo "                Backup only specified id. Can be used multiple times\n\n" 0 "" white 0
    rsw_echo "        -l, --ids\n" 0 "" white 1
    rsw_echo "                List all backup ids\n\n" 0 "" white 0
}

###############################################################################
config="$HOME/.rsync-wrapper.cfg"

mode=""

ids=()
listIds=0
humanReadable=0
verbose=0
help=0

params=("$@")
for idx in "${!params[@]}"; do
    key="${params[$idx]}"
    case $key in
        -i|--id)
            ids+=("${params[$(expr $idx + 1)]}")
        ;;
        -f|--file)
            config="${params[$(expr $idx + 1)]}"
        ;;
        -v|-verbose)
            verbose=$(expr $verbose + 1)
        ;;
        -h|--human-readable)
            humanReadable=1
        ;;
        -l|--ids)
            listIds=1
        ;;
        --help)
            help=1
        ;;
        *)
#            rsw_echo "unknow option $key" "" red
        ;;
    esac
done

if [[ 1 == $help ]]; then
    rsw_help
    exit
fi

#while IFS='' read -r line || [[ -n "$line" ]]; do
while read -u 3 line; do

    IFS='=' read -r -a configLine <<< "$line"
    configKey=${configLine[0]}
#    configValue=$(echo ${configLine[1]})
    configValue=$(echo $line | sed -e 's#[^=]*=##')

    if [[ "$configKey" ==  "CONFIG" ]]; then
        mode="cfg"
    fi

    if [[ "$configKey" ==  "BACKUP" ]]; then
        mode="bkp"

        bkpId=""
        bkpSrc=""
        bkpDsts=()
        bkpExcludes=()
        bkpRsyncOptions=()
        bkpIdentity=""
        bkpPort=22
    fi

    if [[ "$mode" == "cfg" ]]; then
        if [[ $configKey == "excludes"     ]]; then rsw_split "$configValue" configValue;     fi
        if [[ $configKey == "rsyncOptions" ]]; then rsw_split "$configValue" cfgRsyncOptions; fi
    fi

    if [[ "$mode" == "bkp" ]]; then
        if [[ $configKey == "id"           ]]; then bkpId="$configValue";                     fi
        if [[ $configKey == "src"          ]]; then bkpSrc="$configValue";                    fi
        if [[ $configKey == "dst"          ]]; then rsw_split "$configValue" bkpDsts;         fi
        if [[ $configKey == "excludes"     ]]; then rsw_split "$configValue" bkpExcludes;     fi
        if [[ $configKey == "options"      ]]; then rsw_split "$configValue" bkpRsyncOptions; fi
        if [[ $configKey == "identity"     ]]; then bkpIdentity="$configValue";               fi
        if [[ $configKey == "port"         ]]; then bkpPort="$configValue";                      fi


        if [[  1 == $listIds ]]; then
            if [[ $configKey == "id" ]]; then
                rsw_echo "$bkpId\n"
            fi
            continue;
        fi

        if [[ $configKey ==  "//BACKUP" ]]; then
            # clean params & co.
            id="$bkpId"
            src="$(rsw_remove_trailing_slash "$bkpSrc")"
            sshKey="$bkpIdentity"
            sshPort="$bkpPort"
            dsts=()
            for bkpDst in "${bkpDsts[@]}"; do dsts+=("$(rsw_remove_trailing_slash "$bkpDst")"); done
            excludes=()
            for cfgExclude in "${cfgExcludes[@]}"; do excludes+=("$(echo $cfgExclude)"); done
            for bkpExclude in "${bkpExcludes[@]}"; do excludes+=("$(echo $bkpExclude)"); done
            rsyncOptions=()
            for cfgRsyncOption in "${cfgRsyncOptions[@]}"; do rsyncOptions+=("$(echo $cfgRsyncOption)"); done
            for bkpRsyncOption in "${bkpRsyncOptions[@]}"; do rsyncOptions+=("$(echo $bkpRsyncOption)"); done
            if [[ $verbose -gt 1 ]]; then
                rsyncOptions+=("-v");
            else
                rsyncOptions+=("-q");
            fi
            if [[ ! 0 -eq ${#ids[@]} ]]; then
                skip=1
                for i in "${ids[@]}"; do
                    if [[ $i == $id ]]; then
                        skip=0
                    fi
                done
                if [[ 1 -eq $skip ]]; then continue; fi
            fi
            rsw_bkps "$id" "$src" "$(rsw_join "${dsts[@]}")" "$(rsw_join "${excludes[@]}")" "$(rsw_join "${rsyncOptions[@]}")" "$sshKey" "$sshPort"
        fi
    fi
done 3< $config

