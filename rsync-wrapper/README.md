# Readme

## Example :

  - `rsync-wrapper.sh`
  - `rsync-wrapper.sh -f ~/bin/rsw.cfg -h -i bkp-id-1 -i bkp-id-1 -f $HOME/.config.cfg`


## Usage : 

```
NAME
        rsync-wrapper.sh - rsync wrapper

SYNOPSIS
        rsync-wrapper.sh
        rsync-wrapper.sh [-f file] [-v] [-h] [-i | -l]
        rsync-wrapper.sh --help

DESCRIPTION
        -f, --file
                Set an alternative config file. Default is $HOME/.rsw.cfg

        -v, --verbose
                Increase verbosity. Can be used 2 times

        -h, --human-readable
                Human readable sizes

        -i, --id
                Backup only specified id. Can be used multiple times

        -l, --ids
                List all backup ids
```

## Configuration : 

The default config file is `.rsync-wrapper.config` and looks like : 

```
CONFIG
rsyncOptions=--no-links
excludes=cifs*, .Trash*
//CONFIG

BACKUP
id=backup-id
src=/the/source/folderi
dst=/a/backup/folder/, /another/backup/folder/
# excludes=/some/files/to/exclude, /some/dirs/to/exclude/
# identity=/path/to/rsa_file
# port=22
//BACKUP

BACKUP
# ...
//BACKUP
```

Beware the end of each section block `//BACKUP`