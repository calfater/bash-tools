# C_R_*: Color Regular
# C_S_*: Color Strong (bold)
# C_U_*: Color Underlined
# C_B_*: Color Background

# Regular
C_R_BLACK='\[\e[0;30m\]'
C_R_RED='\[\e[0;31m\]'
C_R_GREEN='\[\e[0;32m\]'
C_R_YELLOW='\[\e[0;33m\]'
C_R_BLUE='\[\e[0;34m\]'
C_R_MAGENTA='\[\e[0;35m\]'
C_R_CYAN='\[\e[0;36m\]'
C_R_WHITE='\[\e[0;37m\]'

# Strong
C_S_BLACK='\[\e[1;30m\]'
C_S_RED='\[\e[1;31m\]'
C_S_GREEN='\[\e[1;32m\]'
C_S_YELLOW='\[\e[1;33m\]'
C_S_BLUE='\[\e[1;34m\]'
C_S_MAGENTA='\[\e[1;35m\]'
C_S_CYAN='\[\e[1;36m\]'
C_S_WHITE='\[\e[1;37m\]'

# Underlined
C_U_BLACK='\[\e[4;30m\]'
C_U_RED='\[\e[4;31m\]'
C_U_GREEN='\[\e[4;32m\]'
C_U_YELLOW='\[\e[4;33m\]'
C_U_BLUE='\[\e[4;34m\]'
C_U_MAGENTA='\[\e[4;35m\]'
C_U_CYAN='\[\e[4;36m\]'
C_U_WHITE='\[\e[4;37m\]'

# Background
C_B_BLACK='\[\e[40m\]'
C_B_RED='\[\e[41m\]'
C_B_GREEN='\[\e[42m\]'
C_B_YELLOW='\[\e[43m\]'
C_B_BLUE='\[\e[44m\]'
C_B_MAGENTA='\[\e[45m\]'
C_B_CYAN='\[\e[46m\]'
C_B_WHITE='\[\e[47m\]'

# Reset
C_RESET='\[\e[0m\]'


c_getcolor() {
  case "$1" in
    "black")
      printf "0"
      ;;
    "red")
      printf "1"
      ;;
    "green")
      printf "2"
      ;;
    "yellow")
      printf "3"
      ;;
    "blue")
      printf "4"
      ;;
    "magenta")
      printf "5"
      ;;
    "cyan")
      printf "6"
      ;;
    "white")
      printf "7"
      ;;
    esac
}

c_color_bg()        { tput setab `c_getcolor $1`; }
c_color_bold()      { tput bold; }
c_color_underline() { tput smul; }
c_color_reverse()   { tput rev; }
c_color_reset()     { tput sgr0; }
c_color_text()      { tput setaf `c_getcolor $1`; }

c_color() {
  local text_color=""
  local text_bg=""
  local bold="0"
  local underline="0"
  local reverse="0"
  local escape="0"
  
  for p in $@; do
    case $p in
      '-c' | "-f")
        text_color="$2"
        shift;
      ;;
      "-b")
        text_bg="$2"
        shift;
      ;;
      "-B")
        bold="1"
      ;;
      "-U")
        underline="1"
      ;;
      "-R")
        reverse="1"
      ;;
      "-e")
        escape="1"
      ;;
      "-h" | "--help")
        local bold=$(c_color -B -U)
        local normal=$(c_color)
        local tab="    "

        printf "${bold}NAME${normal}\n"
        printf "${tab}c_color - change shell color\n"
		printf "\n"

        printf "${bold}SYNOPSIS${normal}\n"
        printf "${tab}c_color [-f color|-b color] [-B] [-U] [-R] [-e] [-h|--help]\n"
		printf "\n"

        printf "${bold}DESCRIPTION${normal}\n"
        printf "${tab}If no option is given, reset term's colors.\n"
		printf "\n"
		printf "${tab}Color can be one of: black, red, green, yellow, blue, magenta, cyan, white\n"
		printf "\n"
        printf "${tab}$(c_color)-f color\n"
        printf "${tab}${tab}$(c_color)Set ${bold}f${normal}oreground color\n"
        printf "${tab}$(c_color)-b color\n"
        printf "${tab}${tab}$(c_color)Set ${bold}b${normal}ackground color\n"
        printf "${tab}$(c_color)-B\n"
        printf "${tab}${tab}$(c_color)${bold}B${normal}old\n"
        printf "${tab}$(c_color)-U\n"
        printf "${tab}${tab}$(c_color)${bold}U${normal}nderlined\n"
        printf "${tab}$(c_color)-R\n"
        printf "${tab}${tab}$(c_color)${bold}R${normal}eversed\n"
        printf "${tab}$(c_color)-e\n"
        printf "${tab}${tab}$(c_color)Write ${bold}e${normal}scape sequence\n"
        printf "${tab}$(c_color)-h, --help\n"
        printf "${tab}${tab}$(c_color)Show this ${bold}h${normal}elp\n"
		return
      ;;
      esac
    shift
  done

  [ "1" = "$escape" ] && printf '\['
  c_color_reset
  [ "" != "$text_color" ] && c_color_text "$text_color"
  [ "1" = "$bold" ]  && c_color_bold
  [ "1" = "$underline" ] && c_color_underline
  [ "1" = "$reverse" ] && c_color_reverse
  [ "" != "$text_bg" ] && c_color_bg $text_bg
  [ "1" = "$escape" ] && printf '\]'
}



